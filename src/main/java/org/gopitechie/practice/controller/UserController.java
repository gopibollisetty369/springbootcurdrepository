package org.gopitechie.practice.controller;

import org.gopitechie.practice.domain.User;
import org.gopitechie.practice.dto.UserDto;
import org.gopitechie.practice.service.UserService;
import org.gopitechie.practice.uitils.GTechieController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@GTechieController
public class UserController {

	private static final Logger log= LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/user-registration")
	public String userRegistration(@RequestBody UserDto userDto) {
		
	    User user= userService.userRegistration(userDto);
	    log.info("User Registerd Sucessfully with name {}. done",userDto.getUserName());
	    return "Registerd Sucessfully With Id:" + user.getId();
		
	}
	
}
