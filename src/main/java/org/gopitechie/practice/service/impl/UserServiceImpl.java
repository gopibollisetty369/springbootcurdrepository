package org.gopitechie.practice.service.impl;

import org.gopitechie.practice.domain.User;
import org.gopitechie.practice.dto.UserDto;
import org.gopitechie.practice.repository.UserRepository;
import org.gopitechie.practice.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User userRegistration(UserDto userDto) {

		User user = new User();

		BeanUtils.copyProperties(userDto, user);

		try {
			user = userRepository.save(user);
		} catch (Exception e) {
           System.out.println(e.getStackTrace());
		}
		
		System.out.println("User Saved Sucessfully");
		return user;
	}
}
