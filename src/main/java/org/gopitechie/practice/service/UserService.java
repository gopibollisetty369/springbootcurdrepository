package org.gopitechie.practice.service;

import org.gopitechie.practice.domain.User;
import org.gopitechie.practice.dto.UserDto;

public interface UserService {

	public User userRegistration(UserDto userDto);
	
}
